package com.text1;



import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args){

        new Palindrome();
        System.out.println(Scan());
        new Scanner(System.in).close();
    }
    public static String Scan(){
        Scanner x=new Scanner(System.in);
        boolean whether=isPalindrome(x.nextLong());
//        System.out.println(whether);
        return String.valueOf(whether);
    }
    public static boolean isPalindrome(long x) {
        if (x<0 || (x!=0 && x%10==0)) return false;
        long rev = 0;
        while (x>rev){
            rev = rev*10 + x%10;
            x = x/10;
        }
        return (x==rev || x==rev/10);
    }
}
