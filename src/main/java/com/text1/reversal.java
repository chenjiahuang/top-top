package com.text1;

import java.util.Arrays;
import java.util.Scanner;

public class reversal {
    public static void main(String[] args) {
        System.out.println("请输入：");
        Scanner scanner = new Scanner(System.in);
        String c1=scanner.next();


        char[] c = c1.toCharArray();
        String b = Arrays.toString(c);
        char[] d = new reversal().reverseString(c);
        String e= Arrays.toString(d);
        System.out.println("输入："+b);
        System.out.println("输出："+e);
    }
    public char[] reverseString(char[] s) {

        //双指针
        int begin =0;
        int end =s.length-1;
        while(begin<end){
            //首位和末尾交换位置
            char temp = s[begin];
            s[begin] = s[end];
            s[end] = temp;
            begin++;
            end--;
        }
        return s;
    }
}
